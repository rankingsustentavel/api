# api

The backend of Ranking Sustentável's application, based on NodeJS, Koa, and GraphQL.

## How to run

```bash

# run docker-compose
docker-compose build
docker-compose up

# access via http://localhost:3000 - server running in NodeJS
# Prisma is running at http://localhost:4466
# Prisma is running at http://localhost:27017
```
